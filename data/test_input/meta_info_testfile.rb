# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module Forbin
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  #   Deprecated meta-information:
  #      TESTS_NEED_SELENIUM  -  replaced by aModule.selenium?
  #      PACKAGING - replaced by aModule.packaging # Slightly different, this is an Array of Symbols
  #      WEBAPP - replaced by aModule.webapp?
  #      RUBYFORGE_PROJECT - replaced by aModule.rubyforge_project
  #      PLATFORM - replaced by RUNTIME_VERSIONS
  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  # Required String
  GEM_NAME = "FORBIN"
  # Required String
  VERSION = '0.0.0'
  # Optional String or Array of Strings
  AUTHORS = ["Ignatius J. Reilly"]
  # Optional String or Array of Strings
  EMAILS = ["i.reilly@prometheuscomputing.com"]
  # Optional String
  HOMEPAGE = nil
  # Required String
  SUMMARY = %q{Something really cool}
  # Optional String
  DESCRIPTION = %q{Come on, don't just reproduce the SUMMARY here.' }
  
  # Required Symbol
  # This specifies the language the project is written in (not including the version, which is in LANGUAGE_VERSION).
  # A project should only have one LANGUAGE (not including, for example DSLs such as templating languages).
  # If a project has more than one language (not including DSLs), it should be split.
  # TEMPORARY EXCEPTION: see :frankenstein choice below.
  # The reason is that mixing up languages in one project complicates packaging, deployment, metrics, directory structure, and many other aspects of development.
  # Choices are currently:
  #   * :ruby (implies packaging as gem - contains ZERO java code)
  #   * :java (implies packaging as jar, ear, war, sar, etc (depending on TYPE) - contains ZERO ruby code, with exception of meta_info.rb)
  #   * :frankenstein (implies packaging as gem - contains BOTH ruby and java code - will probably deprecate this in favor of two separate projects)
  LANGUAGE = :ruby
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['> 1.8.1', '< 1.9.3']
  # This is different from aGem::Specification.platform, which appears to be concerned with OS.
  # This defines which implentation of Ruby, Java, etc can be used.
  # Required Hash, in same format as DEPENDENCIES_RUBY.
  # The version part is used by required_ruby_version
  # Allowable keys depend on LANGUAGE. They are in VALID_<language.upcase>_RUNTIMES
  RUNTIME_VERSIONS = {
    :mri => ['> 1.8.1', '< 1.9.3'],
    :jruby => ['1.6.4']
  }
  # Required Symbol
  # Choices are currently:
  #   * :library - reusable functionality, not intended to stand alone
  #   * :utility - intended for use on command line
  #   * :web_app - an application that uses a web browser for it's GUI
  #   * :service - listens on some port. May include command line tools to manage the server.
  #   * :gui - has a Swing, Fox, WXwidget, etc GUI
  TYPE = :utility
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # In the case of JRuby platform Ruby code that depends on a third party Java jar, where do we specify that?
  DEPENDENCIES_RUBY = {
    'rake' => '0.8.7',
    'thin' => '1.2.7'
    }
  DEPENDENCIES_MRI = { 
    'ramaze' => '2011.01.30',
    'prawn' => '0.6.3',
    'indentation' => {:version => ['~> 0.1', '< 0.3']},
    'domain_model' => {:version => '~> 0.0', :source => 'https://user:fakepass@gems.prometheuscomputing.com'}
    }
  DEPENDENCIES_JRUBY = { 
    'ramaze' => '2010.01.02',
    'prawn' => '0.5.7' 
    }
  DEVELOPMENT_DEPENDENCIES_RUBY = { } 
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  require 'Foundation/conventions'
  extend App_MetaInfo_Behaviors
end