require 'optparse'
require_relative 'relative'
# These are informative (not currently used by anything)
$DEVELOPER_PROJECT_ROOTS=Array.new
$USING_DEVELOPER_LOAD_PATHS=false

# Some behaviors useful for working around deficiencies in optparse
class Array
  
  def index_matching(*patterns)
    each_with_index {|value, index|
      return index if patterns.detect {|pat| pat === value }
      }
    nil
  end
  
  # Removes pattern (and optional subsequent values) from receiver.
  # Takes an optional one-argument block which (given a match) returns an integer (the total number of values to remove).
  # Returns:
  #   nil - if there was no match
  #   an Array - if there was a match
  def slice_pat!(*patterns)
    idx = index_matching(*patterns)
    return(nil) unless idx
    num = 1
    num = yield(self[idx]) if block_given?
    slice!(idx, num)
  end
  
  # Removes flag (and optional subsequent argument) from receiver.
  # The subsequent argument is not removed if the flag contains '=', but the result will be split into flag and value.
  # Returns:
  #   nil - if there was no match
  #   an Array - if there was a match
  def remove_option!(with_arg, *patterns)
    result = slice_pat!(*patterns) {|flag| (with_arg && !flag.index('=')) ? 2 : 1 }
    return nil unless result
    if with_arg && 1==result.size
      opt = result.first
      opt_array = opt.split('=')
      result = case opt_array.size
        when 0, 1 ; raise "Impossible condition"
        when 2 ; [opt_array.first, opt_array.last]
        else "Invalid option #{opt.inspect}, contained more than one '='."
      end
    end
    result
  end
  
  # Identical to remove_option!, except:
  # * Always returns an array of arrays resulting from repeated application of remove_option!
  # * If no matches, the returnd array is empty.
  def remove_options!(with_arg, *patterns)
    answer = Array.new
    found = true
    while found
      found = remove_option!(with_arg, *patterns)
      answer << found if found
    end
    answer
  end
  
end


class String
  
  # Presumes the receiver specifies a '/' delimited path relative to one of the entries on $LOAD_PATH.
  # Returns an absolute path that points to the first encounted directory specified the receiver.
  # type can be:
  #   * :file
  #   * :directory
  #   * :either
  # mode can be:
  #   * :strict - an exception is raised if the file or directory cannot be found
  #   * :lax - returns nil if no file or directory can be found
  def find_on_load_path(type=:file, mode=:strict)
    $LOAD_PATH.each {|root|
      path = File.join(root, self)
      next unless File.exist?(path)
      abs = File.absolute_path(path)
      case type
        when :either; return abs
        when :file; return(abs) if File.file?(abs)
        when :directory;  return(abs) if File.directory?(abs)
        else raise "Unexpected type: expected one of [:file, :directory, :either], got #{type.inspect}"
      end
      }
    case mode
      when :strict; raise "Failed to find #{type} #{self.inspect} on $LOAD_PATH, and mode is :strict (you could use :lax instead)."
      when :lax; return nil
      else raise "Unexpected type: expected :file or :dir, got #{type.inspect}"
    end
  end
  
end


# We declare these as constants because they are used twice, once in each level of the parse.
# (in the first level to defining handling, and in the second level to define help text)
# and don't want possibly conflicting duplication
DEV_SWITCH = ['-d', '--dev', "If present, use additional development directories specified by ENV['DEVELOPER_OVERRIDE_DIRECTORIES']"]
APP_SWITCH = ['-a', '--app=REF_IMPL_NAME', String, "Specify the name of a reference implmentation"]
LOAD_SWITCH = ['-l', '--load', String, "Specify additional ruby libraries to place on the load path. Separate paths with colons."]

module Foundation

  # Each colon_delim_dir_list can be a single directory or a colon-delimited list.
  # Individual directories split out from these arguments are provided to prepend_def,
  # and must conform to the description in the comment for that method, which also
  # describes what happens
  # If no argument is provided, ENV['DEVELOPER_OVERRIDE_DIRECTORIES'] is used.
  def self.prepend_dev_dirs(*colon_delim_dir_lists)
    env_dev_dirs = ENV['DEVELOPER_OVERRIDE_DIRECTORIES']
    colon_delim_dir_lists = [env_dev_dirs] if env_dev_dirs && colon_delim_dir_lists.empty?
    return "No development directories to prepend" if colon_delim_dir_lists.empty?
    dev_dirs = colon_delim_dir_lists.collect {|colon_delim_dir_list| colon_delim_dir_list.split(':') }.flatten
    dev_dirs.each {|dev_dir| prepend_dev_dir(dev_dir) }
  end

  # The argument is a path to a single directory (not a colon delimited list of directories) which can contain:
  # * unzipped gems
  # * links to projects under development
  # * Java jars
  # This code puts:
  # * the lib directory for each gem or project on the Ruby $LOAD_PATH
  # * the bin directory for each gem or project on ENV['PATH']
  # * jars in the specified directory on ENV['CLASSPATH']  (not clear that is helpful - probably too late, unless Java is launched by a sub-process)
  def self.prepend_dev_dir(single_directory_path)
    $USING_DEVELOPER_LOAD_PATHS=true
    puts "Prepending development directory: #{single_directory_path.inspect}" if $verbose
    root_pattern = File.join(single_directory_path, '*')
    Dir[root_pattern].each {|root| $DEVELOPER_PROJECT_ROOTS << root }
    lib_pattern = File.join(single_directory_path, '*', 'lib')
    bin_pattern = File.join(single_directory_path, '*', 'bin')
    jar_pattern = File.join(single_directory_path, '*.jar')
    # We sort for the sake of reproducability
    lib_dirs = Dir[lib_pattern].sort!
    bin_dirs = Dir[bin_pattern].sort!
    jar_dirs = Dir[jar_pattern].sort!
    puts "Prepending to $LOAD_PATH: #{lib_dirs.inspect}" if $verbose
    puts "Prepending to ENV['PATH']: #{bin_dirs.inspect}" if $verbose
    puts "Prepending to ENV['CLASSPATH']: #{jar_dirs.inspect}" if $verbose
    exe_paths = [ ENV['PATH'] ]
    class_paths = [ ENV['CLASSPATH'] ]
    lib_dirs.each {|dir| $LOAD_PATH.unshift dir }
    bin_dirs.each {|dir| exe_paths.unshift dir }
    jar_dirs.each {|dir| class_paths.unshift dir }
    ENV['PATH'] = exe_paths.join(':')
    ENV['CLASSPATH'] = class_paths.join(':')
  end

  def self.show_paths
    puts "\n\n$LOAD_PATH:"
    $LOAD_PATH.each {|path| puts "\t#{path.inspect}"}
    puts "\n\n$PATH:"
    ENV['PATH'].split(':').each {|path| puts "\t#{path.inspect}"}
    ENV['CLASSPATH'].split(':').each {|path| puts "\t#{path.inspect}"}
  end

  # ===============================
  # Two level option parse
  # 
  # We do a two-level parse because developer directories need to be added before loading gui_builder.
  # The first level handles (and removes from ARGV):
  # * DEV_SWITCH when revector_development is executed (specify_app is suggested as a convenience)
  # * APP_SWITCH if parse_app_meta_info is optionally executed  (specify_app is suggested as a convenience)
  #
  # The first level is defined in Foundation to reduce redundancy. Revectoring to development directories is a fundamental operation.
  # ===============================


  # Extract from ARGV flags that match any one of the patterns.
  # Patterns can be regular expressions or strings.
  # If include_arg is true, also extracts following value (unless the flag contains '=')
  # Returns a (possibly empty) Array of Arrays (each representing one match).
  # This method exists mostly to document the following problem (because the heavy lifting is done by Array)
  # It would be desirable to use OptionParser itself, thusly:
  #     def self.revector_development
  #       opts = OptionParser.new
  #       opts.on(*DEV_SWITCH) { require 'Foundation/load_path_management'; Foundation.prepend_dev_dirs }
  #       # Note that #parse! removes matching arguments from ARGV. It does not remove other arguments
  #       opts.parse!
  #       raise "ERROR: the root meta_info.rb has not been loaded yet.  Either use the -a option, or programaticlly load the root meta_info.rb before loading this" if include_app_switch && !$ROOT_MODULE
  #     end
  # Unfortunately, this does not work because arguments required at the second level result (at this level) in a OptionParser::InvalidOption.
  # There appears to be no workaround. See: http://stackoverflow.com/questions/3642331/can-optparse-skip-unknown-options-to-be-processed-later-in-a-ruby-program
  def self._extract_from_argv(include_arg, *patterns)
    ARGV.remove_options!(include_arg, *patterns)
  end


  # specify_app will usually be more convenient, but you can run this by itself.
  # Nothing haddens if DEV_SWITCH is false/nil
  # If DEV_SWITCH is present in ARGV:
  # * The optional grandchild_file is useful mainly for ensuring that bin scripts can be run without gem installation.
  # * Development directories are added
  # * DEV_SWITCH is removed
  def self.revector_development(grandchild_file=nil)
    matches = _extract_from_argv(false, '-d', /--dev/)
    return nil if matches.empty?
    raise 'No revectoring here dammit'
    if grandchild_file
      add_project_directory_to_load_path(grandchild_file)
    end
    prepend_dev_dirs
  end
  
  # @file is a path to a file, normally specified by __FILE__
  # The 'lib' folder that @file is found in is added to $LOAD_PATH. 
  #   For example, @file can be a path to a file in 'bin', 'lib', 'features', or 'specs'.
  def self.add_project_directory_to_load_path(file)
    parent = File.dirname(file)
    absolute_parent = File.expand_path(parent)
    lib_dir = File.join(absolute_parent, '..', 'lib')
    $LOAD_PATH << lib_dir unless $LOAD_PATH.include?(lib_dir)
  end

  # Prepend any specified ruby lib paths to the $LOAD_PATH
  def self.prepend_libraries
    matches = _extract_from_argv(true, '-l', /^--load$/)
    matches.each do |match|
      Kernel.warn("Prefer use of lodepath gem with lodepath.yaml instead of adding libraries to command line args!")
      paths = match[1].split(':')
      paths.each {|path| $LOAD_PATH.unshift(File.expand_path(path.strip)) unless path.strip.empty?}
    end
  end

  # Performs first stage of argument parsing:
  # * If -d/--dev flag is present, revectors to development versions (including grandchild_file's aunt lib)
  # * Adds the --app argument to ARGV if app_name is specified (a convenience for app level scripts)
  # * Loads the app's meta_info.rb (if app_name is provided as argument or by ARGV)
  # * Raises an exception if no root (app level) meta_info.rb was loaded.
  # * Keeps --app in ARGV (for subsequent processing at second level) if app_flag_treatment is :non_destructive
  def self.setup_app(app_name=nil, grandchild_file=nil, app_flag_treatment=:destructive)
    prepend_libraries
    if app_name
      ARGV.unshift app_name
      ARGV.unshift '--app'
    end
    add_project_directory_to_load_path(grandchild_file) if grandchild_file
    revector_development
    # Returns the root module
    app_module = parse_app_meta_info(app_flag_treatment)
  end

  # specify_app will usually be more convenient.
  # If APP_SWITCH is present in ARGV, it is removed and the corresponding meta_info.rb is loaded.
  # If APP_SWITCH is not on the command line, this will do nothing other than verify that $ROOT_MODULE is present.
  # If app_flag_treatment is :non_destructive, APP_SWITCH is put back in ARGV. Should only be done when second-level processing must do something other than meta_info load
  def self.parse_app_meta_info(app_flag_treatment=:destructive)
    matches = _extract_from_argv(true, '-a', /--app/)
    unless matches.empty?
      match = matches.first
      app_name = match.last
      app_meta_info = File.join(app_name, 'meta_info')
      meta_info_path = (app_meta_info << ".rb").find_on_load_path
      require app_meta_info
      app_module = Kernel.const_get(extract_module_name_from_meta_info(meta_info_path))
      if :non_destructive==app_flag_treatment
        ARGV.unshift(app_name)
        ARGV.unshift('--app')
      end
    end
    raise "ERROR: the root meta_info.rb has not been loaded yet.  Either use the -a/--app option, or programatically load the root meta_info.rb before loading this" unless $ROOT_MODULE # TODO can we get away from using this global?
    app_module
  end
  
  def self.extract_module_name_from_meta_info(path, silent = false)
    code = File.read(path)
    match = /module (.*)\n/.match(code)
    if !match
      err = "Could not find name of module in #{path.inspect}"
      return err if silent
      raise err
    end
    module_name = match[1]
  end

end

