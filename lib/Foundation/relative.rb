# Returns an absolute path, given a path relative to the file containing the code that sends this message.
# Guts shamelessly stolen from http://rubygems.org/gems/require_relative
def relative(*relative_path_components)
  Kernel.warn("Using #relative from Foundation is deprecated.  Use LodePath's implementation instead.")
  relative_to_nth_caller(relative_path_components, 2)
end

# Require all ruby files directly within a directory. Does not recurse.
def require_dir(path, recurse = false)
  Kernel.warn("Using #require_dir from Foundation is deprecated.  Use LodePath's implementation instead.")
  # NOTE: not using the simpler 'require file' due to a ruby issue where "require 'foo'" and "require 'foo.rb'" will load the same file twice. -- Note: This is not true....MF
  Dir["#{path}/*.rb"].each {|file| 
    require File.join(File.dirname(file), File.basename(file))
  }
end

def relative_to_nth_caller(relative_path_components, caller_depth = 1)
  relative_path_components = [meta_info_path] if relative_path_components.empty?
  file = nth_calling_file(caller_depth + 1)
  case file
  when '(irb)' # When calling relative_to_nth_caller from irb, use PWD as dir
    dir = ENV['PWD']
  when /\A\((.*)\)/ # Raise error if not a valid file path
    raise LoadError, "relative is called in #{$1}"
  else # Normal file path
    dir = File.dirname(file)
  end
  File.expand_path(File.join(*relative_path_components), dir)
end

# Returns the filename of the caller at the given depth in the stack
# caller_depth = 1 will return the file that calls nth_calling_file
def nth_calling_file(caller_depth)
  caller[caller_depth - 1].split(/:\d/,2).first
end
