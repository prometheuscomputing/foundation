require 'fileutils' # For pwd

require 'Foundation/load_path_management'
require 'pp'


# Stolen from http://stackoverflow.com/questions/1032114/check-for-ruby-gem-availability
def gem_available?(name)
   Gem::Specification.find_by_name(name)
rescue Gem::LoadError, LoadError # Must specify, StandardError subclasses are caught by default, but not ScriptError subclasses
   false
rescue
   Gem.available?(name)
end


# Searches for a path to the meta_info.rb file.
# Presumes that:
# * The current directory is the root of the project <--- This is a really bad assumption, and limits the usefulness of this method. -SD
# * Either the name of the project folder is the name of the folder in lib that contains meta_info.rb
#   or else it will look for the meta_info inside all lib/ folders
def meta_info_path
  proj_name = File.basename(FileUtils.pwd)
  path = File.join(".", 'lib', proj_name, 'meta_info.rb')
  return path if File.exist?(path)
  # If path based off name of project folder doesn't work, iterate through lib folders returning the first meta_info.rb
  return Dir['./lib/*/meta_info.rb'].first
end


# Three options for loading a meta_info.rb file:
# * If you want to hard code the module name (or rely on a naming rule), just require the meta_info.rb file like any other file.
# * Use hook_meta_info if you don't want to hard code the module name, and you have a path relative to some root on $LOAD_PATH.
# * Use load_meta_info if you don't want to hard code the module name, and you have a path relative to the current directory, or an absolute path. 
# hook_meta_info and load_meta_info do not rely on name transformation rules.
# 
# This method loads a meta_info.rb file, and returns the Module that holds the meta-info constants.
# If a path is specified, that file is loaded.
# If no path is specified, one is computed by meta_info_path
# If successful, returns the Module containing the meta_info
# On failure:
# * If silent is true, returns a String specifying the error.
# * If silent is false, raises exception.
def load_meta_info(path=nil, silent=false)
  path = path || meta_info_path
  if !File.exist?(path.to_s)
    err =  "Metainfo file #{path.inspect} does not exist"
    return err if silent
    raise err
  end
  # extract module name. Maybe a hook should be used instead.
  module_name = Foundation.extract_module_name_from_meta_info(path, silent)
  begin
    # Parse the meta-information
    load path
  rescue => err
    description = "Could not parse #{path.inspect}. Error: #{err.message}"
    return description if silent
    raise description
  end
  # NOTE: It would be easier to do the following, but const_defined? is broken in some versions of Ruby (at least in ruby 1.8.7 (2010-01-10 patchlevel 249))
  # raise "Failed to lookup module #{module_name}" unless Kernel.const_defined?(module_name)
  begin
    #  Look up the module
    answer = Kernel.const_get(module_name)
    answer.set_project_path_from_module_path(path)
    return answer
  rescue
    err =  "Failed to lookup module #{module_name}"
    return err if silent
    raise err
  end
end

# Please see comment at beginning of load_meta_info.
# Identical to load_meta_info, except:
# * silent is always false
# * loading_path specifies the meta_info.rb file - it is required, and is relative to any root on $LOAD_PATH
# Will raise error if:
# * The module has already been loaded
# * If the loading_path specifies a file without a Module
# * If multiple modules are defined in meta_info.rb
def hook_meta_info(loading_path)
  # This does not literally use a hook, because I've not found one for module creation
  before = Object.constants
  load loading_path
  after = Object.constants
  new_names = after - before
  new_objects = new_names.collect {|name| Object.const_get(name) }
  new_modules = new_objects.select {|mod| mod.instance_of?(Module)}
  raise "Only one Module should defined by #{loading_path}, but there were #{new_modules.size}: #{new_modules.collect {|mod| mod.name } }" if new_modules.size > 1
  raise "Either #{loading_path} has already been loaded, or it contained no modules." if new_modules.empty?
  answer = new_modules.first
  answer.set_project_path_from_module_path(loading_path)
  answer
end
  

# Like load_meta_info, but argument is relative to the file containing the code that sends this message.
# Default argument is 'meta_info.rb', for a sibling file.
def load_meta_relative(*relative_path_components)
  # We do not reuse #relative becuase the caller stack depth would be wrong.
  relative_path_components = [meta_info_path] if relative_path_components.empty?
  file = caller.first.split(/:\d/,2).first
  raise LoadError, "require_relative is called in #{$1}" if /\A\((.*)\)/ =~ file
  path = File.expand_path(File.join(*relative_path_components), File.dirname(file))
  load_meta_info(path)
end


#  normalize_versions puts any one version specification into Array form required by aGem::Specification.add_dependency
#
#  Dependencies can be defined by:
#    A. 'rake' => '> 0.0.1'
#    B. 'rake' => ['> 0.0.1', '< 0.0.3']                 Used when format argument normalize_versions to is :array
#    C. 'rake' => {:version => '0.0.1'}
#    D. 'rake' => {:version => ['> 0.0.1', '< 0.0.3']}   Used when format argument to normalize_versions is :hash (this is the default)

#  No version defined:
#    A. 'rake' => ''
#    B. 'rake' => []
#    C. 'rake' => {}
#    C. 'rake' => {:version => ''}
#    D. 'rake' => {:version => []}


# Can be mixed into things that support to_version_array
module Version_Specifier
  
  # Presumes receiver represents a Ruby gem version
  def to_meta_info_hash; {:version => to_version_array }; end
  
end

class NilClass
  include Version_Specifier
  
  def to_version_array; []; end
  
end

class String
  include Version_Specifier
  
  def to_version_array; empty? ? [] : [self]; end
  
end

class Array
  include Version_Specifier
  
  def to_version_array; self; end
  
  # Presumes receiver represents a Ruby gem version
  def to_meta_info_hash; {:version => to_version_array }; end
  
end

class Hash
  
  def to_version_array; self[:version].to_version_array; end
  
  # Presumes receiver represents a Ruby gem version
  def to_meta_info_hash; 
    raise 'Meta-information hash must at least have :version key' unless has_key?(:version)
    # Duplicate this dependency hash
    meta_info_hash = self.dup
    # Set the version of the duplicated hash to an array built from its previous value
    meta_info_hash[:version]=self[:version].to_version_array
    # Return the duplicated hash
    meta_info_hash
  end
  
  # Presumes receiver is of the form {<gem-name> => <any-version-specification> }.
  # Modifies the receiver, modified to confrom to a given format:
  # When format is :array, converts each entry to form B in above list
  # When format is :hash, converts each entry to form D in above list
  # NOTE:  :array==format does NOT mean the result is an Array, it means that the *version* portion (hash value) is an Array.
  # Returns the receiver.
  def normalize_versions!(format=:hash)
    converter = :hash==format ? :to_meta_info_hash : :to_version_array
    keys.each {|key| self[key]=self[key].send(converter) }
    self
  end
    
end


# I like this functionality being present in Module, but there is danger of conflict. Should reconsider.
class Module
  
  REQUIRED_METAINFO = [:GEM_NAME, :VERSION, :SUMMARY, :LANGUAGE, :RUNTIME_VERSIONS, :TYPE]
  
  # Development dependencies allow for specific versions of RSpec, Cucumber, etc. 
  # I can see that being useful for other people.
  # Internally, when automated testing is available, we can standardize on a specific version (built into MM).
  # I therefore think we do not internally need to use development dependencies.
  
  # This is commented out to avoid double maintenence. See MM::VALID_RUBY_RUNTIMES and MM::VALID_RUBY_RUNTIMES
  # DEPENDENCY_NAMES = [:DEPENDENCIES_RUBY, :DEPENDENCIES_MRI, :DEPENDENCIES_JRUBY, # Need to add other Ruby platfroms
  #                     :DEPENDENCIES_JAVA, # Need to add specific Java platforms
  #                     :DEVELOPMENT_DEPENDENCIES_RUBY, :DEVELOPMENT_DEPENDENCIES_MRI, :DEVELOPMENT_DEPENDENCIES_JRUBY, # Need to add other Ruby platfroms
  #                     :DEVELOPMENT_DEPENDENCIES_JAVA # Need to add specific Java platforms
  #                     ]
  
  # This lets you work with hierarchical collections of dependencies.
  # "platformName" is a parameter, replaced by whichever is appropriate.
  # Keys are the names of collections.
  # Values are Arrays that contain of the names of smaller-collections, or items from DEPENDENCY_NAMES
  # Care must be taken to not create a cycle. The following would create an infinite loop in _concrete_dependency_names:
  #     :A => [:B, :C]
  #     :B => [:A, :D]
  DEPENDENCY_COLLECTIONS = {
    :ALL => [:RUNTIME, :DEVELOPMENT],
    :RUBY_RUNTIME => [:DEPENDENCIES_RUBY, :DEPENDENCIES_platformName],
    :RUBY_DEVELOPMENT => [:DEVELOPMENT_DEPENDENCIES_RUBY, :DEVELOPMENT_DEPENDENCIES_platformName],
    # To simplify meta-information, we do not distinguish between Java compiletime and runtime.
    # Distinguishing between these would simplify the classpath, but that simplification is not essential,
    # and comes at the cost of more complicated meta-info.
    # REMINDER: each project should declare it's own dependencies - it should NOT declare "dependencies of dependencies"
    :JAVA_RUNTIME => [:DEPENDENCIES_JAVA, :DEPENDENCIES_platformName],
    :JAVA_DEVELOPMENT => [:DEVELOPMENT_DEPENDENCIES_JAVA, :DEVELOPMENT_DEPENDENCIES_platformName],
    :RUNTIME => [:JAVA_RUNTIME, :RUBY_RUNTIME],
    :DEVELOPMENT => [:JAVA_DEVELOPMENT, :RUBY_DEVELOPMENT]
  }
  
  # Some gems run on some runtimes, but not others. Dependencies can therefore be different for different runtimes.
  VALID_RUBY_RUNTIMES = [:mri, :jruby, :ironruby, :maglev, :rubinius, :rubyEE, :macruby]
  # We currently distinguish only between 
  #    :java_ME = Micro Edition
  #    :java_SE = Standard Edition
  #    :java_EE = Enterprise Edition
  # There is a list of additional Java virtual machines at http://en.wikipedia.org/wiki/List_of_Java_virtual_machines.
  VALID_JAVA_RUNTIMES = [:java_ME, :java_SE, :java_EE]
  
  RUNTIME_COLLECTIONS = {
    :ruby => VALID_RUBY_RUNTIMES,
    :java => VALID_JAVA_RUNTIMES,
    :all => [:ruby, :java]
  }
  
  # Get specified meta-information. Raise exception if it was required.
  # name is a symbol
  def meta_info(name, default=nil)
    return const_get(name) if const_defined?(name)
    return Module.const_get(name) if Module.const_defined?(name)
    return default unless REQUIRED_METAINFO.member?(name)
    raise "Missing metainformation: #{name.inspect} from module: #{name}"
  end
  
  # ===== metainformation queries: derived information that replaces deprecated constants ==========
  
  def set_project_path_from_module_path(module_path)
    # This presumes the project folder is two steps up from meta_info.rb (3 steps if you think of meta_info.rb itself as a step.)
    @project_path = File.absolute_path(module_path).split(File::SEPARATOR)[0...-3].join(File::SEPARATOR)
  end
  
  def project_path
    raise "project_path is not available. It's set by load_meta_info, hook_meta_info, or set_project_path_from_module_path" unless @project_path && !@project_path.empty?
    @project_path
  end
  
  def project_dir_name
    File.basename(project_path)
  end
  
  # Computed from :TYPE
  def webapp?; :web_app == meta_info(:TYPE); end
  
  # Responds true if webapp? and 'selenium-client' is a key in DEVELOPMENT_DEPENDENCIES_RUBY
  def selenium?
    webapp? && meta_info(:DEVELOPMENT_DEPENDENCIES_RUBY, {}).has_key?('selenium-client')
  end
  
  def ruby?; :ruby == meta_info(:LANGUAGE); end
  def java?; :java == meta_info(:LANGUAGE); end
  
  def gem_name; meta_info(:GEM_NAME); end
  
  # Currently returns one of :gem or :jar, maybe should add :ear, :war, :sar, etc.
  def packaging
    case meta_info(:LANGUAGE)
      when :ruby; [:gem]
      when :java; [:jar] # Maybe add :ear, :war, :sar
      when :frankenstein; [:gem]
    end
  end
  
  # Attempts to extract name of rubyforge project from :HOMEPAGE
  def rubyforge_project
    url = meta_info(:HOMEPAGE)
    return nil unless url
    match = /.*:\/\/rubyforge.org\/projects\/([^\/]+).*/.match(url) || /.*:\/\/(.+).rubyforge.org.*/.match(url)
    match ? match[1] : nil
  end
  
  # ===== utilities ==========
  
  # You probably want to use resolve_dependency_names(*names) instead.
  # This lower level method does not flatten the result, does not remove duplicates, 
  # does not replace 'rubyPlatfrom', does not validate names.
  # This does operate recursively to ensure the results are not keys of DEPENDENCY_COLLECTIONS.
  def _concrete_dependency_names(*names)
    names = names.flatten
    answer = names.collect {|name| DEPENDENCY_COLLECTIONS.has_key?(name) ?  _concrete_dependency_names(*DEPENDENCY_COLLECTIONS[name]) : name }
    answer.flatten.uniq
  end
  
  # Returns a flat Array of platform names
  # platform can be any symbol from RUNTIME_COLLECTIONS.
  def _concrete_platform_names(*platforms)
    platforms = platforms.flatten
    answer = platforms.collect {|platform| RUNTIME_COLLECTIONS.has_key?(platform) ?  _concrete_platform_names(*RUNTIME_COLLECTIONS[platform]) : platform }
    answer.flatten.uniq
  end
  
  # Takes a list of names of dependency types (which can be keys from DEPENDENCY_COLLECTIONS or concrete values formerly in DEPENDENCY_NAMES).
  # platform can be any symbol from RUNTIME_COLLECTIONS (or an Array of such symbols), or nil. If provided, an upcased version replaces 'platformName' in names.
  # Returns a flat array of names of constants (possibly parameterized by platformName).
  # There may be duplicates, and the names are not validated.
  def resolve_dependency_names(platform, *names)
    platforms = _concrete_platform_names(platform)
    platforms.collect {|plat| _resolve_dependency_names(plat, *names) }.flatten.uniq
  end
  
  # Requires resolved platform name.
  def _resolve_dependency_names(platform, *names)
    concrete = _concrete_dependency_names(*names)
    return concrete.reject {|dep| dep.to_s.index('platformName') } unless platform
    p = platform.to_s.upcase
    concrete.collect {|name| name.to_s.gsub('platformName', p).to_sym }
    # Don't want to validate names, because doing so will require maintaining DEPENDENCY_NAMES 
    # bad = concrete.reject {|name| DEPENDENCY_NAMES.member?(name) }
    # raise "Unable to resolve dependency name(s): #{bad.inspect}" unless bad.empty?
  end
  
  # Returns a merged Hash of specified dependencies FOR THIS PROJECT ONLY.
  # Comparison of different dependency extraction methods:
  #   * dependencies           - does not recurse
  #   * recursive_dependencies - recurses over the projects this one depends on (based on *names).  Recursion and extraction are based on the same type of dependency (probably not what you want)
  #   * complete_dependencies  - recurses over the projects this one depends on (based on :ALL in DEPENDENCY_COLLECTIONS) AND THEN iterates over all the modules found during the recursion to extract the final set of dependencies specified by *names.
  # Takes a list of names of dependency types (which can be keys from DEPENDENCY_COLLECTIONS or concrete values formerly in DEPENDENCY_NAMES).
  # platform can be any symbol from RUNTIME_COLLECTIONS (or an Array of such symbols), or nil. If provided, an upcased version replaces 'platformName' in names.
  # format is one of :hash or :array (see normalize_versions!)
  def dependencies(format, platform, *names)
    resolved_names = resolve_dependency_names(platform, *names)
    _dependencies(format, *resolved_names)
  end
  
  # Identical to #dependencies, but requires resolved names
  # platform is no longer an argument because it was needed only for name resolution
  def _dependencies(format, *resolved_names)
    dep = Hash.new
    resolved_names.each {|name| dep.merge! meta_info(name, {}) }
    # Expand constants, environmental variables, and methods, if any. Currently only used for large collections of Java jars.
    this = gem_name
    puts "Computing dependencies for #{this.inspect}. Resolved names: #{resolved_names.inspect}" if $verbose
    dep.clone.each {|name, version|
      name_str = name.to_s
      replacement_dependencies = case name_str
        when /.jar$/
          puts "Project #{this} depends on jar #{name.inspect}" if $verbose
          next
        when /^ENV_(.+)/
          name = $~[1]
          value = ENV[name]
          puts "Project #{this} depends on environmental variable #{name.inspect} (#{value ? 'present' : 'missing'})" if $verbose
          next unless value
          value.split(/\s+/) # Split on whitespace. Maybe should parse, to allow Hash and internal spaces.
        when /^CONST_(.+)/
          const = $~[1]
          present = const_defined?(const)
          puts "Project #{this} depends on constant #{const.inspect} (#{present ? 'present' : 'missing'})" if $verbose
          next unless present
          const_get(const)
        when /^method_(.+)/
          name_sym = $~[1].to_sym
          present = respond_to?(name_sym)
          puts "Project #{this} depends on method #{name_sym.inspect} (#{present ? 'present' : 'missing'})" if $verbose
          next unless present
          send(name_sym)
        else 
          puts "Project #{this} depends on gem or project #{name.inspect}" if $verbose
          next
      end
      dep.delete(name)
      # puts "DELETED #{name.inspect}:"
      # pp dep
      case replacement_dependencies
        when Hash
          puts "Replacement for #{name.inspect} is a Hash" if $verbose
          dep.merge(replacement_dependencies)
        when Array
          puts "Replacement for #{name.inspect} is an Array" if $verbose
          replacement_dependencies.each {|sub_dep| 
          raise "Project #{this} dependency #{name.inspect} replacement is a #{sub_dep.class.name}: #{sub_dep.inspect}" unless sub_dep.kind_of?(String) || sub_dep.kind_of?(Symbol)
          dep[sub_dep]=version 
          }
        else raise "Unexpected type (#{value.class.name}) for dependency #{name.inspect} in #{this}"
      end
      puts "Empty replacement dependency for #{name.inspect} in #{this}" if replacement_dependencies.empty? && $verbose
      }
    answer = dep.normalize_versions!(format)
    pp answer if $verbose
    answer
  end
  
  # Chances are you are looking for complete_dependencies, rather than this, because you often want to traverse based on :ALL, yet collect only one specific kind of dependency (such as gems or jars).
  # See the comment on method #dependencies for a comparison of various dependency calculation methods.
  # This is smilar to #dependencies, but this merges in additional info from other (depended upon) projects (when the project can be found in a sibling directory).
  # This functions by calling _dependencies for this project and every reachable project this depends on, and that is approved by the continue_block.
  # Optional continue_block takes one arg (a module) and computes a Boolean (true if the dependencies of that module should be examined).
  # Returns a three element Array, [success, failure, modules].
  # * The first element is a Hash that contains only dependencies that could be resolved (*their* dependencies will be included).
  # * The second element is a Hash that contains only dependencies that could not be resolved (*their* dependencies will not be included).
  # * The third element is an Array of modules corresponding to the projects that could be resolved (there is a module for every entry in "success" hash - the receiver is *not* included)
  # IMPORTANT PRESUMPTIONS: 
  # * There are no cyclic dependencies (if present, they will cause an endless loop). Cyclic dependencies are a bad thing anyway, and indicate poorly thought out architecture.
  # * The kind of dependency information you want (specified by *names) is the same at all levels
  # * All projects are siblings of the same parent directory. Any project not in the same directory will not be traversed.
  # * Version info is ignored: all those sibling projects must be the correct version (we could issue a warning, but do not yet do so)
  # * We do not need to traverse dependiencies that end in ".jar". Declare dependency on a project instead, if you want to manage those sub-dependencies
  # It would be nice to get rid of these presumptions
  #
  # === example ===
  # irb
  # require 'Foundation/meta_info_processing'; mod = load_meta_info;  found, notFound, modules = mod.recursive_dependencies(:array, :all, :ALL)
  # for project rubyAdapter:
  # found.keys.sort.should == [:Foundation, :common, :plugin_loader] 
  # notFound.keys.sort.should == []
  # modules.collect {|m| m.gem_name}.sort.should == ["Foundation", "common", "plugin_loader", "rubyAdapter"]
  # notFound.keys.collect {|key|File.basename(key)}.sort.should   # 
  def recursive_dependencies(format, platform, *names, &continue_proc)
    resolved_names = resolve_dependency_names(platform, *names)
    _recursive_dependencies(Hash.new, Hash.new, [self], format, *resolved_names, &continue_proc)
  end
  
  # Internal version takes extra args, requires resolved names
  def _recursive_dependencies(success, failure, modules, format, *names, &continue_proc)
    return success, failure, modules if continue_proc && !continue_proc.call(self)
    dep = _dependencies(format, *names)
    dep.each {|dependency, version|
      candidate_project = dependency.to_s
      # Dependency on OUR Java projects should use the project name (without the ".jar" suffix).
      # Dependency on OTHER Java projects (outside of our control) should instead use the ".jar" suffix.
      # We therefore do not attempt to find projects corresponding to jars.
      next if '.jar' == candidate_project[-4..-1]
      # Not clear if this is a project, or an externally provided gem.
      attempt_dir = File.join('..', candidate_project)
      exists = File.exist?(attempt_dir)
      puts "Considering candidate_project: #{candidate_project.inspect} (exists: #{exists})" if $verbose
      next (failure[dependency]=version) unless exists
      mod = nil
      FileUtils.cd(attempt_dir) { mod = load_meta_info(nil, true) }
      case mod
      when Module
         success[dependency]=version
         modules << mod
         mod._recursive_dependencies(success, failure, modules, format, *names, &continue_proc)
      else 
         puts "Unable to find dependencies of #{dependency.inspect}: #{mod.to_s}"
         failure[dependency]=version
      end
      }
    return success, failure, modules
  end
  
  # This finds dependencies of type *dependency_names, based on a PRIOR traveral according to dependency name :ALL in DEPENDENCY_COLLECTIONS
  # See the comment on method #dependencies for a comparison of various dependency calculation methods.
  # This code is motivated by the need to:
  #   * Find jars required (indirectly) by either java or ruby projects
  #     We use Bundler to resolve gem dependencies, but it does not handle dependency on jars (not even for ruby projects).
  #     We could eliminate this if Bundler collected jars into vendor/cache as well as gems.
  #     Michael looked into this, and did not find way to do this with Bundler.
  #     jars themselves (unlike gems) do not include dependency information.
  #     Maven manages dependencies, but more at the MM level than the Bundler level, and would require a different way of specifying dependencies.
  #     ruby-maven is a wrapper on Maven. Another option along these lines is http://mojo.codehaus.org/jruby-maven-plugin/.
  #   * Find out if a java project requires the JRuby runtime 
  #     We could eliminate this by just requiring the JRuby jar.
  #     I'm disinclined to do that, because it amounts to declaring the JRuby dependency multiple times.
  # IMPORTANT PRESUMPTIONS:  see presumptions for recursive_dependencies (which this uses)
  # === example ===
  # irb
  # require 'Foundation/meta_info_processing'; mod = load_meta_info
  # jdep = mod.complete_dependencies(:array, :all, :JAVA_RUNTIME)
  #     jdep.keys.collect {|path| File.basename(path)}.sort.should == ["CaliberRMSDK65.jar", "EccpressoAll.jar", "HTMLEditorLight.jar", "activation.jar", "antlr_2.7.6.1.jar", "axis.jar", "axis.jar", "batik.jar", "brand.jar", "brand_api.jar", "clibwrapper_jiio.jar", "cmof14.jar", "commons-codec-1.3.jar", "commons-discovery-0.2.jar", "commons-httpclient-3.1.jar", "commons-logging-1.0.4.jar", "cvsclient.jar", "flexlm.jar", "freehep-base.jar", "freehep-graphics2d.jar", "freehep-graphicsio-emf.jar", "freehep-graphicsio-ps.jar", "freehep-graphicsio.jar", "jai_imageio.jar", "javax_jmi-1_0-fr.jar", "jax-qname.jar", "jaxb-api.jar", "jaxb-impl.jar", "jaxb-libs.jar", "jaxrpc.jar", "jaxrpc.jar", "jhall.jar", "jide-action.jar", "jide-common.jar", "jide-components.jar", "jide-dock.jar", "jide-editor.jar", "jide-grids.jar", "jide-properties.jar", "jide-shortcut.jar", "jimi.jar", "jmyspell-core-1.0.0-beta-2.jar", "jna.jar", "jsr305.jar", "launcher.jar", "log4j-1.2.15.jar", "md.jar", "md_api.jar", "md_common.jar", "md_common_api.jar", "mdserviceclient.jar", "mdserviceclient.jar", "namespace.jar", "patch.jar", "relaxngDatatype.jar", "rsclient.jar", "saaj.jar", "tw_common.jar", "tw_console.jar", "tw_console_api.jar", "uml2.jar", "velocity-1.6.2-dep.jar", "wsdl4j-1.5.1.jar", "xalan.jar", "xercesImpl.jar", "xml-apis.jar", "xsdlib.jar", "y.jar"]
  # rdep = mod.complete_dependencies(:array, :all, :RUBY_RUNTIME)
  #     rdep.keys.sort.should == [:Foundation, :common, :plugin_loader]
  # modules = Array.new
  # rdep = mod.complete_dependencies(:array, :all, :RUBY_RUNTIME) {|m| modules << m; m.java? }
  #    rdep.keys.sort.should == [:common, :plugin_loader]  # Still has common in it because the final step is to ask traversed modules for :RUBY_RUNTIME
  #    modules.collect {|m| m.gem_name }.sort.should == ["plugin_loader", "rubyAdapter"]  # Does not have common because we did not traverse dependencies of any ruby projects (but we did see plugin_loader)
  def complete_dependencies(format, platform, *dependency_names, &continue_proc)
    _complete_dependencies(format, platform, :ALL, *dependency_names, &continue_proc)
  end
  
  # This separately breaks out traversal_names and dependency_names
  # traversal_names are the (not-yet-resolved) dependencies that are traversed to build the set of project modules.
  # dependency_names are the (not-yet-resolved) dependencies that we want to extract from the traversed projects
  # IMPORTANT PRESUMPTIONS:  see presumptions for recursive_dependencies (which this uses)
  def _complete_dependencies(format, platform, traversal_names, *dependency_names, &continue_proc)
    found, notFound, modules = recursive_dependencies(:array, platform, *traversal_names, &continue_proc)
    resolved_names = resolve_dependency_names(platform, *dependency_names)
    dep = Hash.new
    puts "SECOND PHASE ==== "  if $verbose
    modules.each {|mod| dep.merge! mod._dependencies(format, *resolved_names)  }
    pp dep if $verbose
    dep
  end
  
  # Activates gems specified by hashes identified by names.
  # platform can be :automatic, :mri, :jruby, or nil. If provided, an upcased version replaces 'platformName' in names.
  # Names can be keys from DEPENDENCY_COLLECTIONS or values from DEPENDENCY_NAMES.
  # If no names are provided, :RUBY_RUNTIME will be used.
  # Names must resolve to ruby dependencies.  Most likely the following will suffice:
  # * :RUBY_RUNTIME
  # * :RUBY_DEVELOPMENT
  # Using the above requires that platform is not nil, because they pull in platform specific gems.
  # The following are also valid, and do not require non-nil platform:
  # * :DEPENDENCIES_RUBY
  # * :DEPENDENCIES_MRI
  # * :DEPENDENCIES_JRUBY
  # * :DEVELOPMENT_DEPENDENCIES_RUBY
  # * :DEVELOPMENT_DEPENDENCIES_MRI
  # * :DEVELOPMENT_DEPENDENCIES_JRUBY
  def activate_gems(platform=:all, *names)
    puts "================================================"
    puts "WARNING"
    puts "Gem activation called - activation is deprecated in favor of MM lock + create_app."
    puts "================================================"
    names = [:RUBY_RUNTIME] if names.empty?
    dep = dependencies(:array, platform, *names)
    dep.each {|name, version|
      next unless version && !version.empty?
      puts "Activating: #{name} with requirements: #{version.inspect}"
      gem(name, *version)
      }
  end
  
end

# Path is relative to the file containing the code that sends this message - defults to 'meta_info.rb', for a sibling file.
# If no names are provided, :RUBY_RUNTIME will be used.
def activate_gems(relative_path='meta_info.rb', *names)
  # We do not reuse #relative becuase the caller stack depth would be wrong.
  file = caller.first.split(/:\d/,2).first
  raise LoadError, "require_relative is called in #{$1}" if /\A\((.*)\)/ =~ file
  path = File.expand_path(relative_path, File.dirname(file))
  load_meta_info(path).activate_gems(:all, *names)
end
