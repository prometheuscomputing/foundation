# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module Foundation
  
  # For more information about meta_info.rb, please see project Foundation, lib/Foundation/meta_info.rb
  
  #   Deprecated meta-information:
  #      TESTS_NEED_SELENIUM  -  replaced by aModule.selenium?
  #      PACKAGING - replaced by aModule.packaging # Slightly different, this is an Array of Symbols
  #      WEBAPP - replaced by aModule.webapp?
  #      RUBYFORGE_PROJECT - replaced by aModule.rubyforge_project
  #      PLATFORM - replaced by RUNTIME_VERSIONS
  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  # Required String
  GEM_NAME = "Foundation"
  # Required String
  VERSION = '1.8.2'
  # Optional String or Array of Strings
  AUTHORS = ["Arthur Griesser"]
  # Optional String or Array of Strings
  EMAILS = ["a.griesser@prometheuscomputing.com"]
  # Optional String
  HOMEPAGE = nil
  # Required String
  SUMMARY = %q{Basis of non-trivial Prometheus projects}
  # Optional String
  DESCRIPTION = %q{Foundation provides some shared functionality that reduces code redundancy.
    Currently this includes meta-information declaration and dependency management.' }
  
  # Required Symbol
  # This specifies the language the project is written in (not including the version, which is in LANGUAGE_VERSION).
  # A project should only have one LANGUAGE (not including, for example DSLs such as templating languages).
  # If a project has more than one language (not including DSLs), it should be split.
  # TEMPORARY EXCEPTION: see :frankenstein choice below.
  # The reason is that mixing up languages in one project complicates packaging, deployment, metrics, directory structure, and many other aspects of development.
  # Choices are currently:
  #   * :ruby (implies packaging as gem - contains ZERO java code)
  #   * :java (implies packaging as jar, ear, war, sar, etc (depending on TYPE) - contains ZERO ruby code, with exception of meta_info.rb)
  #   * :frankenstein (implies packaging as gem - contains BOTH ruby and java code - will probably deprecate this in favor of two separate projects)
  LANGUAGE = :ruby
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['> 1.9.2']
  # This is different from aGem::Specification.platform, which appears to be concerned with OS.
  # This defines which implentation of Ruby, Java, etc can be used.
  # Required Hash, in same format as DEPENDENCIES_RUBY.
  # The version part is used by required_ruby_version
  # Allowable keys depend on LANGUAGE. They are in VALID_<language.upcase>_RUNTIMES
  RUNTIME_VERSIONS = {
    :mri => ['> 1.9.2'],
    :jruby => ['1.7.3']
  }
  # Required Symbol
  # Choices are currently:
  #   * :library - reusable functionality, not intended to stand alone
  #   * :utility - intended for use on command line
  #   * :web_app - an application that uses a web browser for it's GUI
  #   * :service - listens on some port. May include command line tools to manage the server.
  #   * :gui - has a Swing, Fox, WXwidget, etc GUI
  TYPE = :library
  
  # Optional, can be :system, :load, :require.  Specifies how to invoke LAUNCHER.
  LAUNCHER_METHOD = nil
  # Required when TYPE is not :utility or :library.
  # Specifies what to invoke when the user double clicks on a packaged application.
  # This is a String path, relative to either bin or lib.
  # Note also ".platypus" alternative.
  LAUNCHER = nil
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # In the case of JRuby platform Ruby code that depends on a third party Java jar, where do we specify that?
  DEPENDENCIES_RUBY = { }
  DEPENDENCIES_MRI = { }
  DEPENDENCIES_JRUBY = { }
  DEPENDENCIES_JAVA = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = { } 
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_JAVA = { }
  
end
