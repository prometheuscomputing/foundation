RUBY_VERSION_ARRAY = RUBY_VERSION.split('.').collect {|str| str.to_i }
expected = false
major = RUBY_VERSION_ARRAY[0]
minor = RUBY_VERSION_ARRAY[1]
[ [1, 8], [1, 9], [2, 0], [2, 1], [2, 2], [2, 3], [2, 4] ].each {|possible_major, possible_minor|
  const_name = "RUBY_#{possible_major}#{possible_minor}"
  match = possible_major==major && possible_minor==minor
  Object.const_set(const_name, match)
  expected = expected || match
  }
RUBY_LANGUAGE_MAJOR_VERSION = "#{major}.#{minor}".to_f # NOTE: "RUBY_LANGUAGE_MAJOR_VERSION" is for backwards compatability - it's a misnomer which was used previously
raise "Unexpected ruby version: #{RUBY_VERSION}" unless expected

