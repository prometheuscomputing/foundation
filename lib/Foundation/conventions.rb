# This defines conventions for file paths, etc.
# Many of these conventions apply to much higher levels, such as gui_builder.
# It may therefore seem incorrect to put this in Foundation. I think, however,
# that Foundation is a reasonable place to define conventions.

# Should be mixed only into the meta-info side of the Module defined in meta_info.rb for the generated project (via "extend Generated_MetaInfo_Behaviors").
# This mixin into module <Project>_Generated is done in <project>_generated/lib/<project>_generated.rb
# Have not yet decided which behaviors belong in the generated module, and which belong in the app module.
# To help determine this, all these methods are all either:
#   * commented "direct", meaning called directly by driver.rb
#   * commented "indirect", meaning called indirectly by driver.rb
#   * commented "called by ..."
#   * deprecated (if method was not called at all when lauching a gui_builder based app)
# Most of these methods have already been implemented in the app module.
# In addition to mixing in these methods, module <Project>_Generated must define methods similar to these:
#   def self.location; # indirect
#     ::File.dirname(__FILE__)
#   end
#   def self.project_dir; # indirect
#     ::File.join(self.lib_dir, 'atmdb_generated')
#   end
module Generated_MetaInfo_Behaviors
  
  def launch_jruby_drb_server; raise "Deprecated"
    load jruby_drb_server_file
  end
  
  def launch_ruby_drb_server; raise "Deprecated"
    load ruby_drb_server_file
  end
  
  def launch_drb_client; raise "Deprecated"
    load drb_client_file
  end
  
  def load_ruby_api; raise "Deprecated"
    require driver_file
  end
  
  def load_jruby_api; raise "Deprecated"
    require jruby_driver_file
  end
  
  # Path getter methods
  
  def data_dir; # indirect
    p_gen_name = ::File.split(project_dir).last # project_dir is hard coded in the <Project>_Generated module by the code generator, currently in <project>_generated.rb 
    data_dir = ::File.join(::File.expand_path('~'), 'Prometheus', 'data', p_gen_name)  # Does ~ work on Windows?
    # Create if non-existant
    ::FileUtils.mkpath(data_dir)
    data_dir
  end
    

  def lib_dir; # indirect
    location
  end
      
  def model_dir; # direct
    ::File.join(project_dir, 'model')
  end
  
  def java_dir; raise "Deprecated"
    ::File.join(project_dir, 'java')
  end

  def jruby_driver_file; raise "Deprecated"
    ::File.join(java_dir, 'jruby.rb')
  end
  
  def ruby_remote_dir; raise "Deprecated"
    ::File.join(project_dir, 'ruby_remote')
  end
  
  def jruby_drb_server_file; raise "Deprecated"
    ::File.join(ruby_remote_dir, 'launch_jruby_drb_server.rb')
  end
  
  def ruby_drb_server_file; raise "Deprecated"
    ::File.join(ruby_remote_dir, 'launch_ruby_drb_server.rb')
  end
  
  def drb_client_file; raise "Deprecated"
    ::File.join(ruby_remote_dir, 'client.rb')
  end
  
  def spec_file; # called from gui_builder/ramaze/server_config.rb (not driver)
    ::File.join(model_dir, 'spec.rb')
  end
    
  def driver_file; # called from gui_builder/ramaze/server_config.rb (not driver)
    ::File.join(model_dir, 'driver.rb')
  end
  
  def db_location; # direct
    @db_location ||= ::File.join(data_dir, 'SQL.db')
  end
  
  # This should not be deprecated. We would have no other way of specifying an alternate db_location for
  # code that relies on the generated driver.rb file. -SD
  # driver.rb isn't used anymore so this can be deprecated. =- MF
  # The gui_builder '-b' flag relies on this method.
  def db_location=(db_location) ; raise "Deprecated"
    @db_location = db_location
  end
  
end

# Should be mixed only into the meta-info side of the Module defined in meta_info.rb for the application itself (via "extend App_MetaInfo_Behaviors")
# The highest level (root) project, that contain hand written code
module App_MetaInfo_Behaviors
  
  def self.included(mixin_recipient)
    raise "App_MetaInfo_Behaviors should only be mixed into the class side of a single Module (using 'extend'), defined in the root project's meta_info.rb"
  end
  
  def self.extended(mixin_recipient)
    return if $ROOT_MODULE == mixin_recipient
    raise "App_MetaInfo_Behaviors should be mixed only into the class side of a single Module (using 'extend'), defined in the root project's meta_info.rb. It's already mixed into #{$ROOT_MODULE.name}, and now it's being mixed into #{mixin_recipient.name}" if $ROOT_MODULE
    $ROOT_MODULE = mixin_recipient
    $META_INFO_PATH = nth_calling_file(2) # the file in which #extend was called
    # puts "meta_info file found at #{$META_INFO_PATH}"
    # puts $META_INFO_PATH.split("/")[-4] # should be name of project dir
    # puts $META_INFO_PATH.split("/")[-3] # should be "lib"
    # puts $META_INFO_PATH.split("/")[-2] # should be same as name of project dir
  end
  
  # ==== Gem, module, and file names ====
  
  # The root module is the one that contains all the meta-information in meta_info.rb for the "root project" based on the ref impl gen.
  #    By convention, this is also the "Module for code generation" prompted for by the MagicDraw "Generate Reference Implementation" menu item.
  #    Even though you are prompted for "Module for code generation", it's actually generated_module_name that gets created.  Edit: You shouldn't be prompted anymore.  If you are doing things correctly then the name of the model will be in the project_name tag for the model that you are generating.
  def root_module_name; name; end
  
  # The following items have different meaning, even though they have the same sequence of characters.
  def project_name; $ROOT_MODULE::GEM_NAME; end
  alias_method :root_main_file_name, :project_name # The file used to load the gem. Does not include extension.
  alias_method :root_gem_name, :project_name # Base name, does not include version, platform, or extension
  
  def generated_project_name; "#{project_name}_generated"; end 
  alias_method :generated_gem_name, :generated_project_name # Base name, does not include version, platform, or extension
  def generated_main_file_name; "#{root_main_file_name}_generated"; end # The file used to load the gem. Does not include extension.
  
  # The convention has changed so that there is no longer an underscore.  See #legacy_generated_module_name.  Foundation codebase only has one usage of #generated_module_name and it was fixed up to look for the module name using both the old and new conventions.  Hopefully this will suffice.
  def generated_module_name
    generated_module.to_s
  end
  
  def spec_name; "#{generated_main_file_name}.rb"; end
  def spec_title
    root_module_name.split(/(?=[A-Z][a-z])|(?<=[a-z])(?=[A-Z0-9]+$)|_+|\s+/).join(" ")
  end
  def custom_spec_path; ::File.join(project_name, 'custom_spec.rb'); end # this assumes there is a single custom spec file, which is not necessarily true anymore.  TODO: fix this
  def model_extensions_path; ::File.join(project_name, 'model_extensions.rb'); end
  def schema_extensions_dir; ::File.join(project_name, 'schema_extensions'); end
  def processes_dir; ::File.join(project_name, 'processes'); end

  def load_generated
    # This loads <project>_generated.rb
    # There is not very much in that file: all the behaviors in that file should be added to App_MetaInfo_Behaviors
    result = require generated_main_file_name
    puts("Load generated: #{result.inspect}") if $verbose
  end

  def generated_module
    load_generated
    result = Object.const_get("#{root_module_name}Generated") rescue nil
    result ||= Object.const_get("#{root_module_name}_Generated") rescue nil
    raise "Failed to find generated module #{generated_module_name} or #{legacy_generated_module_name}" unless result
    result
  end

  # This peforms load_generated and also loads <project>_generated/model/driver.rb.
  def load_local_ruby_api
    # generated_module.load_ruby_api # Deprecated behaviors on generated module
    load_generated # will be removed
    load_ruby_api  # may need load_jruby_api -  FIX - should not need to distinguish
  end
  
  # You will also need to mirror_module or use_module for modules containing domain classes.
  # Do not use mirror_module or use_module for ChangeTracker or Gui_Builder_Profile, because
  # that may result in confusion between proxies to different servers, when multiple servers
  # are used.  Instead use #change_tracker and #rich_text
  def load_remote_ruby_api
    # result = generated_module.launch_drb_client
    result = load 'telemetheus_client.rb'
    puts("Launch drb client: #{result.inspect}") if $verbose
  end

  def launch_remote_api_server
    # generated_module.launch_ruby_drb_server
    generated_module.load_ruby_api
    # Need to load facade definitions and their implmentation code (but not yet doing so)
    load 'telemetheus_server.rb'  
  end
  
  # What is the point of this??
  def default_app_options
    raise "Module #{name} does not define DEFAULT_APP_OPTIONS" unless const_defined?('DEFAULT_APP_OPTIONS')
    const_get('DEFAULT_APP_OPTIONS')
  end
  
  # =======================================================
  # Backwards compatability
  #
  # This code used to be placed in <project>_generated.rb
  # These methods used to be available in the generated module.
  # There was no strong reason for that. 
  # It often required extra steps to find the generated module (possibly need to load it first)
  # Now the methods are available directly on the root project module
  # =======================================================
  
  def launch_jruby_drb_server
    load jruby_drb_server_file
  end
  
  def launch_ruby_drb_server
    load ruby_drb_server_file
  end
  
  def launch_drb_client
    load drb_client_file
  end
  
  def load_ruby_api
    require driver_file
  end
  
  def load_jruby_api
    require jruby_driver_file
  end
  
  # Path getter methods
  
  def data_dir
    data_dir = ::File.join(::File.expand_path('~'), 'Prometheus', 'data', generated_project_name)
    # Create if non-existant
    ::FileUtils.mkpath(data_dir)
    data_dir
  end
    
  def location
    # ::File.dirname(__FILE__)
    raise "Deprecated in favor of $LOAD_PATH relative paths"
  end

  def lib_dir
    # location
    raise "Deprecated in favor of $LOAD_PATH relative paths"
  end
    
  # This is the generated project
  def project_dir
    # ::File.join(lib_dir, generated_project_name)
    raise "Deprecated in favor of $LOAD_PATH relative paths"
  end
  
  def model_dir
    ::File.join(generated_project_name, 'model')
  end
  
  def java_dir
    ::File.join(generated_project_name, 'java')
  end

  def jruby_driver_file
    ::File.join(java_dir, 'jruby.rb')
  end
  
  def ruby_remote_dir
    # ::File.join(generated_project_name, 'ruby_remote')
    raise "Deprecated in favor of separate Telemetheus projects. See instructions in telemetheus_client/doc_source"
  end
  
  def jruby_drb_server_file
    # ::File.join(ruby_remote_dir, 'launch_jruby_drb_server.rb')
    raise "Deprecated in favor of separate Telemetheus projects. See instructions in telemetheus_client/doc_source"
  end
  
  def ruby_drb_server_file
    # ::File.join(ruby_remote_dir, 'launch_ruby_drb_server.rb')
    raise "Deprecated in favor of separate Telemetheus projects. See instructions in telemetheus_client/doc_source"
  end
  
  def drb_client_file
    ::File.join(ruby_remote_dir, 'client.rb')
  end
  
  def spec_file
    ::File.join(model_dir, 'spec.rb')
  end
    
  def driver_file
    ::File.join(model_dir, 'driver.rb')
  end
  
  def db_location
    @db_location ||= ::File.join(data_dir, 'SQL.db')
  end
  
  def db_location=(db_location)
    @db_location = db_location
  end

end

# ============================================

def root_module
  raise "The $ROOT_MODULE has not yet been defined: you need to load meta_info.rb for the root project (with 'include App_MetaInfo_Behaviors')" unless $ROOT_MODULE
  $ROOT_MODULE
end

def default_app_options
  warn "[DEPRECATION] calling `default_app_options` defined on Object should be avoided.  `default_app_options` is defined on the root module (which is returned by Foundation.setup_app) so you should call it directly on that instead."
  root_module.default_app_options
end

# This will be deprecated once the behaviors in <project>_generated.rb have been moved into App_MetaInfo_Behaviors
def load_generated; root_module.load_generated; end

# To access the local API, all you need to do is run this method:
#    require 'xyz/meta_info.rb'
#    load_local_ruby_api
def load_local_ruby_api; root_module.load_local_ruby_api; end

# See documentation in telemetheus_client
def launch_remote_api_server; root_module.launch_remote_api_server; end
