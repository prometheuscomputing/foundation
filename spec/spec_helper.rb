$:.unshift File.expand_path('../../lib', __FILE__)
require File.expand_path('../path_test/path_test.rb', __FILE__)

# Enable old syntax for Rspec 3.0
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end