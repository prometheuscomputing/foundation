# rspec -I ./lib --color --backtrace ./specs/spec_*
require 'spec_helper'
require 'Foundation/load_path_management'

# Ugly computation to get expected value
project = File.dirname(File.dirname(__FILE__))
rel_root = File.join(project, 'data', 'test_input', 'dev_dirs', 'manhattan', 'lib')
EXPECTED_LOAD_ROOT = File.absolute_path(rel_root)

describe Foundation, " can" do
  
  it "prepend additional directories to the load path" do;
    Foundation.prepend_dev_dirs('./data/test_input/dev_dirs')
    Object.const_defined?(:SOME_FOOLISHLY_NAMED_CONSTANT).should == false
    require 'manhattan'
    Object.const_defined?(:SOME_FOOLISHLY_NAMED_CONSTANT).should == true
    SOME_FOOLISHLY_NAMED_CONSTANT.should == 'Wombats from the fifth dimension'
  end
  
  it "should be able to process a relative path to a file into an absolute path via #relative" do
    dependencies_spec_file_path = File.expand_path(File.join(File.dirname(__FILE__), 'dependencies_spec.rb'))
    relative('dependencies_spec.rb').should == dependencies_spec_file_path
    relative('../spec/dependencies_spec.rb').should == dependencies_spec_file_path
    relative('..', 'spec', 'dependencies_spec.rb').should == dependencies_spec_file_path
    relative(['..', 'spec', 'dependencies_spec.rb']).should == dependencies_spec_file_path
    relative_to_nth_caller('dependencies_spec.rb').should == dependencies_spec_file_path
    relative_to_nth_caller('../spec/dependencies_spec.rb', 1).should == dependencies_spec_file_path
    relative_to_nth_caller(['..', 'spec', 'dependencies_spec.rb'], 1).should == dependencies_spec_file_path
    # Test applications of relative_to_nth_caller at different depths
    relative_to_calling_file('dependencies_spec.rb').should == dependencies_spec_file_path
    relative_to_path_test_file('../dependencies_spec.rb').should == dependencies_spec_file_path
  end
  
end

describe String, " representing $LOAD_PATH relative path can" do
  
  # PRESUMES ABOVE SPEC PUT data/dev_dirs/manhattan/lib on $LOAD_PATH
  it "be converted to an absolute path to a file" do;
    abs = 'manhattan.rb'.find_on_load_path
    abs.should == File.join(EXPECTED_LOAD_ROOT, 'manhattan.rb')
    expect {'bronx'.find_on_load_path}.to raise_error("Failed to find file \"bronx\" on $LOAD_PATH, and mode is :strict (you could use :lax instead).")
    'bronx'.find_on_load_path(:file, :lax).should be_nil
  end
  
  # PRESUMES ABOVE SPEC PUT data/dev_dirs/manhattan/lib on $LOAD_PATH
  it "be converted to an absolute path to a directory" do;
    abs = 'bronx'.find_on_load_path(:directory)
    abs.should == File.join(EXPECTED_LOAD_ROOT, 'bronx')
    expect {'manhattan.rb'.find_on_load_path(:directory) }.to raise_error("Failed to find directory \"manhattan.rb\" on $LOAD_PATH, and mode is :strict (you could use :lax instead).")
    'manhattan.rb'.find_on_load_path(:directory, :lax).should be_nil
  end
  
end

describe Array, " can" do
  
  it "find index of first entry matching one of several patterns, which can be String or Regexp" do;
    ["foo", "bar", "baz", 'bif', 'boom'].index_matching('zif').should be_nil
    ["foo", "bar", "baz", 'bif', 'boom'].index_matching('zif', 'baz').should == 2
    ["foo", "bar", "baz", 'bif', 'boom'].index_matching('zif', /ba/).should == 1
  end
  
  it "remove first match (and optional subsequent values) to any of several patterns" do;
    receiver=["foo", "bar", "baz", 'bif', 'boom']; result = receiver.slice_pat!(/za/)
    receiver.should == ["foo", "bar", "baz", 'bif', 'boom']
    result.should == nil
    receiver=["foo", "bar", "baz", 'bif', 'boom']; result =  receiver.slice_pat!(/ba/)
    receiver.should == ["foo", "baz", 'bif', 'boom']
    result.should == ['bar']
    receiver=["foo", "bar", "baz", 'bif', 'boom']; result =  receiver.slice_pat!(/ba/) {|val| 2}
    receiver.should == ["foo", 'bif', 'boom']
    result.should == ['bar', 'baz']
  end
  
  it "Removes flag (and optional subsequent argument), handling flag=value correctly" do;
    receiver=["foo", "bar", "baz", 'bif', 'boom']; result = receiver.remove_option!(true, /za/)
    receiver.should == ["foo", "bar", "baz", 'bif', 'boom']
    result.should == nil
    
    receiver=["foo", "bar", "biz", 'bif', 'boom']; result = receiver.remove_option!(true, /ba/)
    receiver.should == ["foo", 'bif', 'boom']
    result.should == ['bar', 'biz']
    
    receiver=["foo", "bar=5", "biz", 'bif', 'boom']; result = receiver.remove_option!(true, /ba/)
    receiver.should == ["foo", "biz", 'bif', 'boom']
    result.should == ['bar', '5']
    
    receiver=["foo", "bar", "baz", 'bif', 'boom']; result = receiver.remove_option!(false, /ba/)
    receiver.should == ["foo", "baz", 'bif', 'boom']
    result.should == ['bar']
    
    receiver=["foo", "bar=5", "baz", 'bif', 'boom']; result = receiver.remove_option!(false, /ba/)
    receiver.should == ["foo", "baz", 'bif', 'boom']
    result.should == ['bar=5'] # Does not decompose because there is not supposed to be an argument
  end
  
  it "Removes multiple flags (and optional subsequent arguments), handling flag=value correctly" do;
    receiver=["foo", "bar=5", "baz", 'bif', 'boom', 'crunch']
    result = receiver.remove_options!('zif', /ba/)
    receiver.should == ["foo", 'boom', 'crunch']
    result.should == [  ["bar", "5"],  ["baz", 'bif'] ]
  end
  
end