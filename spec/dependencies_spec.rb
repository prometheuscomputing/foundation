# rspec -I ./lib --color --backtrace ./specs/spec_*

require 'Foundation/meta_info_processing'

# NOTE: adding this to not fail the check to ensure that multiple modules aren't defined when loading meta_info.rb
# Without this two modules are loaded in addition to the one defined in meta_info: 
#  * Generated_MetaInfo_Behaviors
#  * App_MetaInfo_Behaviors
# I'm not sure why it doesn't fail during normal meta_info loading -SD
require 'Foundation/conventions'

describe Kernel, " initially" do
  it "finds meta_info.rb file for project" do;
    meta_info_path.should == './lib/Foundation/meta_info.rb'
  end
  it "loads a meta_info.rb file (relative to $LOAD_PATH) and returns the Module" do;
    $LOAD_PATH.unshift './data'
    # This must be run before the load_module test
    mod = hook_meta_info('test_input/meta_info_testfile.rb')
    mod.should be_instance_of(Module)
    mod.name.should == 'Forbin'
  end
  it "loads a meta_info.rb file (relative to the current directory) and returns the Module" do;
    mod = load_meta_info('./data/test_input/meta_info_testfile.rb')
    mod.should be_instance_of(Module)
    mod.name.should == 'Forbin'
  end
end

describe Module, " when meta_info.rb is loaded" do
  before(:all) { @mod = load_meta_info('./data/test_input/meta_info_testfile.rb') }
  it "knows project path" do;
    abs = @mod.project_path
    rel = abs.gsub(FileUtils.pwd, '.')
    rel.should == "."
  end
  it "finds mri dependencies" do;
    dep = @mod.dependencies(:array, :mri, :RUBY_RUNTIME)
    # Shared dependencies
    dep['rake'].should == ['0.8.7']
    dep['thin'].should == ['1.2.7']
    # MRI specific dependencies
    dep['ramaze'].should == ['2011.01.30']
    dep['prawn'].should == ['0.6.3']
    # MRI hash-specified dependencies
    dep['indentation'].should == ['~> 0.1', '< 0.3']
    dep['domain_model'].should == ['~> 0.0']
  end
  it "finds mri dependencies (hash return value)" do
    dep = @mod.dependencies(:hash, :mri, :RUBY_RUNTIME)
    # Shared dependencies
    dep['rake'].should == {:version => ['0.8.7']}
    dep['thin'].should == {:version => ['1.2.7']}
    # MRI specific dependencies
    dep['ramaze'].should == {:version => ['2011.01.30']}
    dep['prawn'].should == {:version => ['0.6.3']}
    # MRI hash-specified dependencies
    dep['indentation'].should == {:version => ['~> 0.1', '< 0.3']}
    dep['domain_model'].should == {:version => ['~> 0.0'], :source => 'https://user:fakepass@gems.prometheuscomputing.com'}
  end
  it "finds jruby dependencies" do;
    dep = @mod.dependencies(:array, :jruby, :RUBY_RUNTIME)
    # Shared dependencies
    dep['rake'].should == ['0.8.7']
    dep['thin'].should == ['1.2.7']
    # JRuby specific dependencies
    dep['ramaze'].should == ['2010.01.02']
    dep['prawn'].should == ['0.5.7']
  end
  # This functionality doesn't seem to be used and doesn't work, so I'm commenting out the test for it -SD
  # it "sets $META_INFO_PATH" do
  #   $META_INFO_PATH.should == File.expand_path("../../data/test_input/meta_info_testfile.rb", __FILE__)
  # end
end