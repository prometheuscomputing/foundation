# Return a path relative to the caller
def relative_to_calling_file(path)
  relative_to_nth_caller(path, 2)
end

# Return a path relative to this file.
def relative_to_path_test_file(path)
  relative_to_nth_caller(path, 1)
end